var mongoose = require('mongoose'),
    mongooseBelongsto = require('mongoose-belongsto'),
    mongoosePolymorphic = require('mongoose-polymorphic'),
    Promise = require('bluebird');

/*
 * feedCollection
 *
 * Plugin to make a collection into a news feed store
 */
module.exports = function(schema, options){
  options.actorType = options.actorType || 'User';
  options.actorId = options.actorId || 'actor';

  schema.plugin(mongooseBelongsto, {collection: options.actorType, name: options.actorId, promise: Promise});
  schema.plugin(mongoosePolymorphic, {associationKey: 'source', required: true, promise: Promise});

  var schemaAdditions = { visibility: {type: 'String'} };

  schemaAdditions[options.actorId] = {
    type: mongoose.Schema.ObjectId,
    ref: options.actorType,
    required: true
  };
  schema.add(schemaAdditions);


  /*
   * newFromSourceAndUser - build a new instance attached to a source
   * @param {Object} source
   * @param {User} user
   * @returns {FeedItem}
   */
  schema.statics.newFromSourceAndUser = function(item, user, callback){
    var FeedItemModel = this;

    function buildFeedItem(feedItem){
      if (!feedItem) {
        var feedItem = new FeedItemModel();
      }
      feedItem[options.actorId] = user;
      feedItem.sourceId = item.id;
      feedItem.sourceType = item.constructor.modelName;
      feedItem.visibility = item.visibility;
      return feedItem;
    }

    var lookupParams = {sourceId: item.id};
    lookupParams[options.actorId] = user;

    return this.findOne(lookupParams).execAsync().then(buildFeedItem).nodeify(callback);
  }


  /*
   * createFromSourceAndUser - create a new instance attached to a source
   * @param {Object} source
   * @param {User} user
   * @returns {FeedItem}
   */
  schema.statics.createFromSourceAndUser = function(item, user, callback){
    saveFeedItem = function(feedItem){
      return feedItem.saveAsync().get(0);
    };

    return this.newFromSourceAndUser(item, user).then(saveFeedItem).nodeify(callback);
  }
}
