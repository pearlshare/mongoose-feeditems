var mongoose = require('mongoose');

/*
 * Tracker
 *
 * Plugin to add functionality to track a collection and create news feed items
 */
module.exports = function(schema, options){
  options.feedCollection = options.feedCollection || 'FeedItem';
  options.actorId = options.actorId || 'user'

  /*
   * Callbacks
   */
  schema.pre('save', function(next){
    this.createFeedItem(next);
  });

  schema.pre('remove', function(next){
    this.removeFeedItem(next);
  });

  /*
   * Methods
   */
  schema.methods.createFeedItem = function(callback){
    return mongoose.model(options.feedCollection).createFromSourceAndUser(this, this[options.actorId]).nodeify(callback);
  }

  schema.methods.removeFeedItem = function(callback){
    return mongoose.model(options.feedCollection).findOneAndRemove({sourceId: this.id, sourceType: this.constructor.modelName}).execAsync().nodeify(callback);
  }
}