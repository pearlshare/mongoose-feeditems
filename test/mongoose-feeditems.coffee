mocha = require("mocha")
chai = require("chai")
mongooseFeeditems = require('../index')
mongoose = require('mongoose')
expect = chai.expect


###
  Setup test environment
###

mongoose.connect('mongodb://localhost/mongoose-feeditems')
mongoose.connection.on 'error', (err) ->
  console.error "MongoDB error:", err
  console.error 'Make sure a mongoDB server is running and accessible by this application'

# User
UserSchema = mongoose.Schema
  name: 'String'
mongoose.model 'User', UserSchema

# Thing
ThingToTrackSchema = mongoose.Schema
  name: 'String'
  user:
    type: mongoose.Schema.ObjectId
    ref: 'User'
  visibility:
    type: "String"
ThingToTrackSchema.plugin mongooseFeeditems.tracker, feedCollection: 'FeedItem'
mongoose.model 'ThingToTrack', ThingToTrackSchema

# Feed Item
FeedItemSchema = mongoose.Schema({})
FeedItemSchema.plugin mongooseFeeditems.feedCollection, actorType: 'User'
mongoose.model 'FeedItem', FeedItemSchema



###
  Usage
###
User = mongoose.model("User")
ThingToTrack = mongoose.model("ThingToTrack")
FeedItem = mongoose.model("FeedItem")

user = new User(name: "Bob")
thing = new ThingToTrack(name: 'Fish', user: user, visibility: "friends")
feedItem = new FeedItem

describe 'Mongoose feeditems', ->

  context 'When the tracker plugin is applied to the Thing model', ->

    it 'should have a createFeedItem method', (done) ->
      expect(thing).to.have.property("createFeedItem")
      done()

    it 'should have a removeFeedItem method', (done) ->
      expect(thing).to.have.property("removeFeedItem")
      done()

    it 'should save a feedItem', (done) ->
      FeedItem.count (err, initialCount) ->
        thing = new ThingToTrack(name: 'Fish', user: user)
        thing.save (err) ->
          FeedItem.count (err, newCount) ->
            expect(newCount).to.equal(initialCount + 1)
            done(err)

    it 'should remove a feedItem on destroy', (done) ->
      thing = new ThingToTrack(name: 'Fish', user: user)

      thing.save (err) ->
        FeedItem.count (err, oldCount) ->
          thing.remove (err) ->
            FeedItem.count (err, count) ->
              expect(count).to.equal(oldCount - 1)
              done()


  context 'When the feedCollection plugin is applied to the FeedItem model', ->

    it 'should have an actor', ->
      expect(feedItem.schema.paths).to.have.property("actorId")

    it 'should have an sourceId', ->
      expect(feedItem.schema.paths).to.have.property("sourceId")

    it 'should have an sourceType', ->
      expect(feedItem.schema.paths).to.have.property("sourceType")
