var expect = require("expect.js"),
    mongooseFeeditems = require('../index'),
    mongoose = require('mongoose');


/*
  Setup test environment
 */

mongoose.connect('mongodb://localhost/mongoose-feeditems');

mongoose.connection.on('error', function(err) {
  console.error("MongoDB error:", err);
  return console.error('Make sure a mongoDB server is running and accessible by this application');
});

var UserSchema = mongoose.Schema({
  name: 'String'
});

mongoose.model('User', UserSchema);

var ThingToTrackSchema = mongoose.Schema({
  name: 'String',
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  visibility: {
    type: "String"
  }
});

ThingToTrackSchema.plugin(mongooseFeeditems.tracker, {
  feedCollection: 'FeedItem'
});

mongoose.model('ThingToTrack', ThingToTrackSchema);

var FeedItemSchema = mongoose.Schema({});

FeedItemSchema.plugin(mongooseFeeditems.feedCollection, {
  actorType: 'User'
});

mongoose.model('FeedItem', FeedItemSchema);


/*
  Usage
 */

var User = mongoose.model("User");

var ThingToTrack = mongoose.model("ThingToTrack");

var FeedItem = mongoose.model("FeedItem");

var user = new User({
  name: "Bob"
});

var thing = new ThingToTrack({
  name: 'Fish',
  user: user,
  visibility: "friends"
});

var feedItem = new FeedItem;

describe('Mongoose feeditems', function() {
  context('When the tracker plugin is applied to the Thing model', function() {
    it('should have a createFeedItem method', function(done) {
      expect(thing).to.have.property("createFeedItem");
      return done();
    });
    it('should have a removeFeedItem method', function(done) {
      expect(thing).to.have.property("removeFeedItem");
      return done();
    });
    it('should save a feedItem', function(done) {
      return FeedItem.count(function(err, initialCount) {
        thing = new ThingToTrack({
          name: 'Fish',
          user: user
        });
        return thing.save(function(err) {
          return FeedItem.count(function(err, newCount) {
            expect(newCount).to.equal(initialCount + 1);
            return done(err);
          });
        });
      });
    });
    return it('should remove a feedItem on destroy', function(done) {
      thing = new ThingToTrack({
        name: 'Fish',
        user: user
      });
      return thing.save(function(err) {
        return FeedItem.count(function(err, oldCount) {
          return thing.remove(function(err) {
            return FeedItem.count(function(err, count) {
              expect(count).to.equal(oldCount - 1);
              return done();
            });
          });
        });
      });
    });
  });
  return context('When the feedCollection plugin is applied to the FeedItem model', function() {
    it('should have an actor', function() {
      return expect(feedItem.schema.paths).to.have.property("actorId");
    });
    it('should have an sourceId', function() {
      return expect(feedItem.schema.paths).to.have.property("sourceId");
    });
    return it('should have an sourceType', function() {
      return expect(feedItem.schema.paths).to.have.property("sourceType");
    });
  });
});