var Promise = require('bluebird'),
    mongoose = require('mongoose');

Promise.promisifyAll(mongoose);

exports.feedCollection = require("./lib/feedCollection");
exports.tracker = require("./lib/tracker");