# Mongoose FeedItems

A simple news feed system for a mongoose.js backed node.js application.

## How it works

Creates a FeedItem collection whose documents have the following attributes:

* actor - the user who caused the feed item to be created
* createdAt - a timestamp of when the item was created
* sourceId - the mongoDB id of the source item
* sourceType - the name of the collection containing the source item

Adds methods and callbacks to other models which create and remove feedItems when they are created or destroyed.

## Example

```coffee
#
# Setup models
#

mongooseFeeditems = require("mongoose-feeditems")

mongoose = require("mongoose")

ThingToTrackSchema = mongoose.Schema.new
  name: 'String'

ThingToTrackSchema.plugin mongooseFeedItem.tracker, feedCollection: 'FeedItem'

mongoose.model 'ThingToTrack', ThingToTrackSchema


FeedItemSchema = mongoose.Schema.new()

FeedItemSchema.plugin mongooseFeedItem.feedCollection

#
# Use em
#  
ThingToTrack = mongoose.model("ThingToTrack")
FeedItem = mongoose.model("FeedItem")

thing = new ThingToTrack
  name: 'Fish'

thing.save (err) ->
  console.log 'thing saved!'

  FeedItem.find({}).exec (err, feedItems) ->
    console.log 'feedItems', feedItems # => will contain a feedItem with the source set to the thing.

    feedItems[0].fetchSource (err, source) ->

      console.log 'source', source # => will print the thing 



```
